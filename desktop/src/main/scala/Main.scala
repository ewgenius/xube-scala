package com.xube

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl._
import com.voxel.VoxelGame
import com.xonixgame.XonixGame
import com.xube.XubeGame


object Main extends App {
  val cfg = new LwjglApplicationConfiguration
  cfg.title = "xube-scala"
  cfg.height = 600
  cfg.width = 1024
  cfg.forceExit = false
  new LwjglApplication(new VoxelGame, cfg)
  //new LwjglApplication(new XonixGame, cfg)
}
