package com.xonixgame.screens

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.{Gdx, ScreenAdapter}
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.xonixgame.XonixGame
import com.xonixgame.systems.BoardSystem
import com.xube.systems.{ScriptSystem, Render3dSystem}

/**
 *
 */
class GameScreen(private val game: XonixGame) extends ScreenAdapter {
  val modelBatch: ModelBatch = game.modelBatch
  val stage: Stage = game.stage

  val cameraController = new CameraInputController(game.perspectiveCamera)
  Gdx.input.setInputProcessor(cameraController)

  init()

  def init(): Unit = {
    val boardWidth = 10
    val boardHeight = 10
    val boardX = 0
    val boardZ = 0

    game.perspectiveCamera.position.set(10, 10, 10)
    game.perspectiveCamera.lookAt(0, 0, 0)
    game.perspectiveCamera.near = 1
    game.perspectiveCamera.far = 1000

    game.environment.set(new ColorAttribute(ColorAttribute.AmbientLight, Color.LIGHT_GRAY))
    val light: DirectionalLight = new DirectionalLight
    light.set(Color.WHITE, new Vector3(-1, -1, 1))
    game.environment.add(light)

    game.engine.addSystem(new BoardSystem(boardWidth, boardHeight, boardX, boardZ))
    game.engine.addSystem(new Render3dSystem(modelBatch, game.perspectiveCamera, game.environment))
    game.engine.addSystem(new ScriptSystem(game.scriptManager))


    game.world.createBoard(boardWidth, boardHeight)
    for (i <- Array(1, 2, 3, 4, 5, 6, 7))
      game.world.createBall()

  }

  override def render(deltaTime: Float): Unit = {
    cameraController.update()
    game.engine.update(deltaTime)
  }
}
