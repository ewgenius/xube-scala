package com.xonixgame.components

import com.badlogic.ashley.core.Component

/**
 * Created by ewgenius on 13.03.15.
 */

object CellState {

}

class CellComponent extends Component {
  var state: Int = 0
  var x: Int = 0
  var z: Int = 0

  def setState(value: Int): Unit = {
    state = value
  }
}
