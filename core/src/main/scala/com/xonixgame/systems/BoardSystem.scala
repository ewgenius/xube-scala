package com.xonixgame.systems

import com.badlogic.ashley.core.{ComponentMapper, Entity, Family}
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.math.Vector3
import com.xonixgame.components.CellComponent
import com.xube.components.{ModelComponent, TransformComponent, VelocityComponent}
import scala.collection.mutable

class BoardSystem(var boardWidth: Int,
                  var boardHeight: Int,
                  var boardX: Float,
                  var boardZ: Float) extends IteratingSystem(Family.all(classOf[VelocityComponent], classOf[TransformComponent]).get()) {
  private val itemQuery = new mutable.ListBuffer[Entity]

  private val velocityMapper = ComponentMapper.getFor(classOf[VelocityComponent])
  private val transformMapper = ComponentMapper.getFor(classOf[TransformComponent])

  var board = Array.ofDim[Entity](boardWidth, boardHeight)

  def checkCell(x: Int, z: Int): Int = {
    if (x >= 0 && x < boardWidth && z >= 0 && z < boardHeight) {
      val cell = board(z)(x)
      cell.getComponent(classOf[ModelComponent]).model.materials.first().set(ColorAttribute.createDiffuse(Color.RED))
      cell.getComponent(classOf[CellComponent]).state
    } else {
      -1
    }
  }

  override def update(deltaTime: Float): Unit = {
    super.update(deltaTime)

    for (row <- board) {
      for (cell <- row) {
        val state = cell.getComponent(classOf[CellComponent]).state
        cell.getComponent(classOf[ModelComponent]).model.materials.first().set(ColorAttribute.createDiffuse(if (state == 0) Color.YELLOW else Color.GREEN))
      }
    }

    itemQuery.foreach((entity: Entity) => {
      val velocity = velocityMapper.get(entity)
      val transform = transformMapper.get(entity)

      val x = transform.position.x
      val z = transform.position.z
      val dx = x + velocity.velocity.x
      val dz = z + velocity.velocity.z


      // check filled cells
      val ix = (x + 15).toInt
      val iz = (z + 15).toInt

      val dix = if (dx - x > 0) 1 else -1
      val diz = if (dz - z > 0) 1 else -1

      if (//checkCell(ix + dix, iz - 1) != 0 ||
        checkCell(ix + dix, iz) != 0) //||
        //checkCell(ix + dix, iz + 1) != 0)
        velocity.velocity.x *= -1

      if (//checkCell(ix - 1, iz + diz) != 0 ||
        checkCell(ix, iz + diz) != 0)// ||
        //checkCell(ix + 1, iz + diz) != 0)
        velocity.velocity.z *= -1

      // check other balls
      /*itemQuery.foreach((entity2: Entity) => {
        if (!entity2.equals(entity)) {
          val velocity2 = velocityMapper.get(entity2)
          val transform2 = transformMapper.get(entity2)
          val x2 = transform2.position.x
          val z2 = transform2.position.z

          val r = Math.abs(x2 - x) + Math.abs(z2 - z)
          if (r <= 2) {
            val t = new Vector3(velocity.velocity)
            velocity.velocity = new Vector3(velocity2.velocity)
            velocity2.velocity = t
          }
        }
      })*/

      val dv = new Vector3(velocity.velocity)

      transform.position.add(dv.scl(deltaTime))
    })
    itemQuery.clear()
  }

  override def processEntity(entity: Entity, deltaTime: Float) = {
    itemQuery += entity
  }
}