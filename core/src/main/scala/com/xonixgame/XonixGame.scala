package com.xonixgame

import com.xonixgame.components.CellComponent
import com.xube.XubeGame
import com.xonixgame.screens.GameScreen
import com.xube.components.TransformComponent

class XonixGame extends XubeGame {
  private var _world: World = null
  
  def world = _world

  override def create(): Unit = {
    super.create()
    
    scriptManager.set("TransformComponent", classOf[TransformComponent])
    scriptManager.set("CellComponent", classOf[CellComponent])

    //scriptManager.runConsole()
    
    _world = new World(engine)
    
    setScreen(new GameScreen(this))
  }
}
