package com.xonixgame

import java.lang.Math

import com.badlogic.ashley.core.{Engine, Entity}
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.graphics.g3d.{Material, ModelInstance}
import com.badlogic.gdx.graphics.{Color, VertexAttributes}
import com.badlogic.gdx.math.Vector3
import com.xonixgame.components.CellComponent
import com.xonixgame.systems.BoardSystem
import com.xube.components.{VelocityComponent, ModelComponent, ScriptComponent, TransformComponent}

/**
 * *
 * @param engine
 */
class World(private val engine: Engine) {

  private val modelBuilder = new ModelBuilder

  val boardWidth = 30

  def createBoard(boardWidth: Int, boardHeight: Int): Entity = {
    val entity = new Entity

    val transform = new TransformComponent

    //val model = new ModelComponent
    //model.model = new ModelInstance(
    //  modelBuilder.createLineGrid(
    //    boardWidth, boardHeight, 1, 1,
    //    new Material(
    //      ColorAttribute.createDiffuse(Color.WHITE)),
    //    VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal)
    //)

    entity.add(transform)

    engine.addEntity(entity)

    val boardSystem = engine.getSystem(classOf[BoardSystem])

    for (x <- 0 until boardWidth)
      for (z <- 0 until boardHeight) {
        val state = if ((x == 0 || x == boardWidth - 1) || (z == 0 || z == boardHeight - 1) || Math.random() > 0.8) 1 else 0
        boardSystem.board(z)(x) = createSquare(x - boardWidth / 2, z - boardHeight / 2, state)
      }

    entity
  }

  def createSquare(x: Int, z: Int, state: Int): Entity = {
    val entity = new Entity

    val transform = new TransformComponent
    transform.position.x = x
    transform.position.z = z
    transform.scale.y = 0.1f

    val cell = new CellComponent
    cell.state = state
    cell.x = x
    cell.z = z

    val script = new ScriptComponent
    script.script =
      """
        |
        | var transform = entity.getComponent(TransformComponent);
        | var cell = entity.getComponent(CellComponent);
        | var state = cell.state()
        | if(state == 1) {
        |   transform.scale().y += 0.2 * deltaTime ;
        | } 
        | if(transform.scale().y >= 4) {
        |   cell.setState(2);
        | }   
        |
      """.stripMargin

    val model = new ModelComponent
    model.model = new ModelInstance(
      modelBuilder.createBox(1f, 1f, 1f,
        new Material(
          ColorAttribute.createDiffuse(Color.YELLOW)),
        VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal)
    )

    entity.add(transform).add(model).add(script).add(cell)

    engine.addEntity(entity)

    entity
  }

  def createBall(): Entity = {
    val entity = new Entity

    val transform = new TransformComponent
    transform.position.y = 0.5f
    transform.position.x = (Math.random().toFloat - 0.5f) * 20
    transform.position.z = (Math.random().toFloat - 0.5f) * 20

    val model = new ModelComponent
    model.model = new ModelInstance(
      modelBuilder.createSphere(1, 1, 1, 20, 20,
        new Material(
          ColorAttribute.createDiffuse(Color.RED)),
        VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal)
    )

    val velocity = new VelocityComponent
    velocity.velocity = new Vector3(Math.random().toFloat, 0, Math.random().toFloat).nor().scl(2)

    entity.add(model).add(transform).add(velocity)

    engine.addEntity(entity)

    entity
  }

  def createPlayer(): Entity = {
    val entity = new Entity

    val transform = new TransformComponent
    transform.position.y = 0.5f

    val model = new ModelComponent
    model.model = new ModelInstance(
      modelBuilder.createSphere(1, 1, 1, 20, 20,
        new Material(
          ColorAttribute.createDiffuse(Color.RED)),
        VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal)
    )

    val velocity = new VelocityComponent
    velocity.velocity = new Vector3(Math.random().toFloat, 0, Math.random().toFloat).nor().scl(10)

    entity.add(model).add(transform).add(velocity)

    engine.addEntity(entity)

    entity
  }

  def createTestEntity(): Entity = {
    val entity = new Entity

    val script = new ScriptComponent
    script.script =
      """
        |
        | var position = entity.getComponents().get(0).position();
        | position.x += 0.2 * delta;
        |
      """.stripMargin

    val transform = new TransformComponent

    val model = new ModelComponent
    model.model = new ModelInstance(modelBuilder.createBox(1f, 1f, 1f,
      new Material(ColorAttribute.createDiffuse(Color.GREEN)),
      VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal))

    entity
      .add(transform)
      .add(model)
      .add(script)

    engine.addEntity(entity)

    entity
  }
}