package com.xube

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.{Gdx, Game}
import com.badlogic.gdx.graphics.{Color, PerspectiveCamera}
import com.badlogic.gdx.graphics.g3d.{Environment, ModelBatch}
import com.badlogic.gdx.scenes.scene2d.Stage

class XubeGame extends Game {
  private var _scriptManager: ScriptManager = null
  private var _modelBatch: ModelBatch = null
  private var _stage: Stage = null
  private var _engine: Engine = null
  private var _perspectiveCamera: PerspectiveCamera = null
  private var _environment = new Environment

  def scriptManager = _scriptManager

  def modelBatch = _modelBatch

  def stage = _stage

  def engine = _engine

  def perspectiveCamera = _perspectiveCamera
  
  def environment = _environment

  override def create(): Unit = {
    _scriptManager = new ScriptManager(this)
    _modelBatch = new ModelBatch()
    _stage = new Stage()
    _engine = new Engine()

    _perspectiveCamera = new PerspectiveCamera(67, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
  }

  override def dispose(): Unit = {
    _modelBatch.dispose()
    _stage.dispose()
    _scriptManager.dispose()
  }
}
