package com.xube.systems

import com.badlogic.ashley.core.{ComponentMapper, Family, Entity}
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20, Camera}
import com.badlogic.gdx.graphics.g3d.{Environment, ModelBatch}
import com.xube.components.{ModelComponent, TransformComponent}
import scala.collection.mutable.ListBuffer


class Render3dSystem(
                    private val modelBatch: ModelBatch,
                    private val camera: Camera,
                    private val environment: Environment
                    ) extends IteratingSystem(Family.all(classOf[TransformComponent], classOf[ModelComponent]).get()) {

  private val renderQuery = new ListBuffer[Entity]
  private val modelMapper = ComponentMapper.getFor(classOf[ModelComponent])
  private val transformMapper = ComponentMapper.getFor(classOf[TransformComponent])

  override def update(deltaTime: Float): Unit = {
    super.update(deltaTime)

    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)

    camera.update()
    modelBatch.begin(camera)

    renderQuery.foreach((entity: Entity) => {
      val model = modelMapper.get(entity)
      val transform = transformMapper.get(entity)
      
      model.model.transform.set(transform.position, transform.orientation, transform.scale)
      modelBatch.render(model.model, environment)
    })

    modelBatch.end()
    renderQuery.clear()
  }

  override def processEntity(entity: Entity, deltaTime: Float): Unit = {
    renderQuery += entity
  }
}
