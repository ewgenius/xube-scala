package com.xube.systems

import com.badlogic.ashley.core.{ComponentMapper, Entity, Family}
import com.badlogic.ashley.systems.IteratingSystem
import com.xube.ScriptManager
import com.xube.components.ScriptComponent
import scala.collection.mutable

/**
 * Created by ewgenius on 10.03.15.
 */
class ScriptSystem(
                    private val scriptManager: ScriptManager
                    ) extends IteratingSystem(Family.all(classOf[ScriptComponent]).get()) {
  private val scriptQuery = new mutable.ListBuffer[Entity]
  private val scriptMapper = ComponentMapper.getFor(classOf[ScriptComponent])

  override def update(deltaTime: Float): Unit = {
    super.update(deltaTime)
    
    scriptQuery.foreach((entity: Entity) => {
      val script = scriptMapper.get(entity)
      scriptManager.set("deltaTime", deltaTime)
      scriptManager.set("entity", entity)
      scriptManager.eval(script.script)
    })
    scriptQuery.clear()
  }

  override def processEntity(entity: Entity, deltaTime: Float) = {
    scriptQuery += entity
  }
}
