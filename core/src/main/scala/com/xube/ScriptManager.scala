package com.xube

import java.io.{InputStreamReader, BufferedReader}
import javax.script.{ScriptEngine, ScriptEngineManager}

import com.badlogic.gdx.utils.Disposable

/**
 * *
 */
class ScriptManager(private val game: XubeGame) extends Disposable {
  private val engineManager: ScriptEngineManager = new ScriptEngineManager()
  private val engine: ScriptEngine = engineManager.getEngineByName("javascript")
  
  set("console", System.out)

  def set[T](name: String, value: T): Unit = {
    engine.put(name, value)
  }

  /**
   * eval js string * 
   * @param script valid js code
   * @return
   */
  def eval(script: String): Object = {
    try {
      engine.eval(script)
    } catch {
      case e: Exception =>
        println(e)
        null
    }
  }

  /**
   * run js REPL * 
   */
  def runConsole(): Unit = {
    try {
      val reader: BufferedReader = new BufferedReader(new InputStreamReader(System.in))
      while (true) {
        print(">: ")
        val s: String = reader.readLine()
        val result = eval(s)
        if (result != null)
          println(result)
      }
    }
  }

  override def dispose(): Unit = {

  }
}
