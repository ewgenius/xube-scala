package com.xube.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math._

/**
 * *
 */
class ModelComponent extends Component {
  var model:ModelInstance = null
}
