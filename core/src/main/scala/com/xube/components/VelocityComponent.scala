package com.xube.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector3

/**
 * *
 */
class VelocityComponent extends Component {
  var velocity: Vector3 = new Vector3
}
