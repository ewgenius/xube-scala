package com.xube.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math._

/**
 * *
 */
class ScriptComponent extends Component {
  var script: String = ""
}
