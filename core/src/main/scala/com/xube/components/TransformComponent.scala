package com.xube.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math._

/**
 * *
 */
class TransformComponent extends Component {
  var position: Vector3 = new Vector3()
  var orientation: Quaternion = new Quaternion()
  var scale: Vector3 = new Vector3(1, 1, 1)
}
