package com.voxel

import com.badlogic.gdx.graphics.{GL20, Color, VertexAttribute, Mesh}
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.{Material, RenderableProvider, Renderable}
import com.badlogic.gdx.utils

class VoxelNode(
                 private val voxelSize: Float,
                 private val chunkSizeX: Int,
                 private val chunkSizeY: Int,
                 private val chunkSizeZ: Int,
                 private val chunksX: Int,
                 private val chunksY: Int,
                 private val chunksZ: Int
                 ) extends RenderableProvider {

  private val chunksCount = chunksX * chunksY * chunksZ
  private val chunksSize = chunkSizeX * chunkSizeY * chunkSizeZ
  var renderedChunks = 0

  // init chunks
  private val chunks: Array[VoxelChunk] = new Array[VoxelChunk](chunksCount)
  var i = 0
  for (y <- 0 until chunksY) {
    for (z <- 0 until chunksZ) {
      for (x <- 0 until chunksX) {
        val chunk = new VoxelChunk(chunkSizeX, chunkSizeY, chunkSizeZ)
        chunk.offset.set(x * chunkSizeX, y * chunkSizeY, z * chunkSizeZ)
        chunks(i) = chunk
        i += 1
      }
    }
  }

  // flags
  private val dirty: Array[Boolean] = Array.fill(chunksCount)(false)

  // vertices counts
  private val numVertices: Array[Int] = Array.fill(chunksCount)(0)

  // vertices
  private val vertices: Array[Float] = Array.fill(VoxelChunk.vertexSize * 6 * chunksSize)(0)

  // materials and meshes
  private val materials: Array[Material] = new Array[Material](chunksCount)
  private val meshes: Array[Mesh] = new Array[Mesh](chunksCount)
  for (i <- 0 until meshes.length) {
    meshes(i) = new Mesh(true, chunksSize * 6 * 4, chunksSize * 12, VertexAttribute.Position(), VertexAttribute.Normal())
    materials(i) = new Material(ColorAttribute.createDiffuse(Color.RED))
  }

  override def getRenderables(renderables: utils.Array[Renderable], pool: utils.Pool[Renderable]): Unit = {
    renderedChunks = 0
    for (i <- 0 until chunks.length) {
      val chunk: VoxelChunk = chunks(i)
      val mesh = meshes(i)
      if (dirty(i)) {
        val numVerts = chunk.calculateVertices(vertices)
        numVertices(i) = numVerts / 4 * 6
        mesh.setVertices(vertices, 0, numVerts * VoxelChunk.vertexSize)
        dirty(i) = false
      }

      val renderable = pool.obtain()
      renderable.material = materials(i)
      renderable.mesh = mesh
      renderable.meshPartOffset = 0
      renderable.meshPartSize = numVertices(i)
      renderable.primitiveType = GL20.GL_TRIANGLES
      renderables.add(renderable)
      renderedChunks += 1
    }
  }
}
