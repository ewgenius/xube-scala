package com.voxel

import com.xube.XubeGame

class VoxelGame extends XubeGame {
  override def create(): Unit = {
    super.create()
    setScreen(new GameScreen(this))
  }
}
