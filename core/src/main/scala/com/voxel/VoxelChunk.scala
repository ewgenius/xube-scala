package com.voxel

import com.badlogic.gdx.math.Vector3

object VoxelChunk {
  val vertexSize: Int = 6

  def createTop(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
  def createBottom(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
  def createLeft(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
  def createRight(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
  def createFront(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
  def createBack(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int, voxelSize: Float): Int = {
    var vo = vertexOffset

    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x + 1
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = offset.x + x
    vertices({vo+=1; vo-1}) = offset.y + y + 1
    vertices({vo+=1; vo-1}) = offset.z + z + 1
    vertices({vo+=1; vo-1}) = 0
    vertices({vo+=1; vo-1}) = 1
    vertices({vo+=1; vo-1}) = 0

    vo
  }
}

class VoxelChunk(
                  val width: Int,
                  val height: Int,
                  val depth: Int,
                  val voxelSize: Float
                  ) {
  private val length = width * height * depth
  val voxels: Array[Byte] = Array.fill(length)(1)
  val offset: Vector3 = new Vector3()
  private val topOffset = width * depth
  private val bottomOffset = -topOffset
  private val leftOffset = -1
  private val rightOffset = 1
  private val frontOffset = -width
  private val backOffset = width
  private val widthTimesHeight = width * height

  def getFast(x: Int, y: Int, z: Int): Byte = {
    if (x >= 0 && x < width &&
      y >= 0 && y < height &&
      z >= 0 && z < depth) {
      voxels(x + z * width + y * widthTimesHeight)
    } else {
      -1
    }
  }

  def setFast(x: Int, y: Int, z: Int, voxel: Byte): Unit = {
    voxels(x + z * width + y * widthTimesHeight) = voxel
  }

  def calculateVertices(vertices: Array[Float]): Int = {
    var i = 0
    var vertexOffset = 0
    for (y <- 0 until height) {
      for (z <- 0 until depth) {
        for (x <- 0 until width) {
          val voxel: Byte = voxels(i)
          if (voxel != 0) {
            if (y < height - 1) {
              if (voxels(i + topOffset) == 0) vertexOffset = VoxelChunk.createTop(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createTop(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            if (y > 0) {
              if (voxels(i + bottomOffset) == 0) vertexOffset = VoxelChunk.createBottom(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createBottom(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            if (x > 0) {
              if (voxels(i + leftOffset) == 0) vertexOffset = VoxelChunk.createLeft(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createLeft(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            if (x < width - 1) {
              if (voxels(i + rightOffset) == 0) vertexOffset = VoxelChunk.createRight(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createRight(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            if (z > 0) {
              if (voxels(i + frontOffset) == 0) vertexOffset = VoxelChunk.createFront(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createFront(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            if (z < depth - 1) {
              if (voxels(i + backOffset) == 0) vertexOffset = VoxelChunk.createBack(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
            else {
              vertexOffset = VoxelChunk.createBack(offset, x, y, z, vertices, vertexOffset, voxelSize)
            }
          }
          i += 1
        }
      }
    }
    vertexOffset / VoxelChunk.vertexSize
  }
}
