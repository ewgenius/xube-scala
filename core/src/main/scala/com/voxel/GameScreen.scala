package com.voxel

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}
import com.badlogic.gdx.graphics.g3d.{ModelInstance, Material, Renderable, ModelBatch}
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.{MeshBuilder, CameraInputController}
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.{Gdx, ScreenAdapter}

class GameScreen(private val game: VoxelGame) extends ScreenAdapter {
  val modelBatch: ModelBatch = game.modelBatch
  val stage: Stage = game.stage
  val spriteBatch: SpriteBatch = new SpriteBatch
  val bitmapFont: BitmapFont = new BitmapFont

  val cameraController = new CameraInputController(game.perspectiveCamera)
  Gdx.input.setInputProcessor(cameraController)

  val voxelNode = new VoxelNode(1,2,2,2,2,2,2)

  init()

  def init(): Unit = {
    game.perspectiveCamera.position.set(10, 10, 10)
    game.perspectiveCamera.lookAt(0, 0, 0)
    game.perspectiveCamera.near = 1
    game.perspectiveCamera.far = 1000

    game.environment.set(new ColorAttribute(ColorAttribute.AmbientLight, Color.LIGHT_GRAY))
    val light: DirectionalLight = new DirectionalLight
    light.set(Color.WHITE, new Vector3(-1, -1, 1))
    game.environment.add(light)
  }

  override def render(deltaTime: Float): Unit = {
    super.render(deltaTime)

    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    cameraController.update()
    game.perspectiveCamera.update(true)

    modelBatch.begin(game.perspectiveCamera)
    modelBatch.render(voxelNode, game.environment)
    modelBatch.end()

    spriteBatch.begin()
    bitmapFont.draw(spriteBatch, "fps: " + Gdx.graphics.getFramesPerSecond, 10, 40)
    bitmapFont.draw(spriteBatch,  "chunks: " + voxelNode.renderedChunks, 10, 20)
    spriteBatch.end()
  }
}