package com.voxelgame

import com.badlogic.gdx.math.Vector3

/**
 * *
 * @param width
 * @param height
 * @param depth
 */
class VoxelChunk(val width: Int, val height: Int, val depth: Int) {
  val voxels: Array[Byte] = Array.fill(width * height * depth)(0)
  val offset: Vector3 = new Vector3()
  private val widthTimesHeight: Int = width * height
  private val topOffset: Int = width * depth
  private val bottomOffset: Int = -width * depth
  private val leftOffset: Int = -1
  private val rightOffset: Int = 1
  private val frontOffset: Int = -width
  private val backOffset: Int = width

  def set(x: Int, y: Int, z: Int, voxel: Byte) {
    if (x < 0 || x >= width) return
    if (y < 0 || y >= height) return
    if (z < 0 || z >= depth) return
    setFast(x, y, z, voxel)
  }

  def setFast(x: Int, y: Int, z: Int, voxel: Byte) {
    voxels(x + z * width + y * widthTimesHeight) = voxel
  }

  def calculateVertices(vertices: Array[Float]): Int = {
    var i = 0
    var vertexOffset = 0
    for (y <- 0 until height) {
      for (z <- 0 until depth) {
        for (x <- 0 until width) {
          val voxel: Byte = voxels(i)
          if (voxel != 0) {
            if (y < height - 1) {
              if (voxels(i + topOffset) == 0) vertexOffset = createTop(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createTop(offset, x, y, z, vertices, vertexOffset)
            }
            if (y > 0) {
              if (voxels(i + bottomOffset) == 0) vertexOffset = createBottom(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createBottom(offset, x, y, z, vertices, vertexOffset)
            }
            if (x > 0) {
              if (voxels(i + leftOffset) == 0) vertexOffset = createLeft(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createLeft(offset, x, y, z, vertices, vertexOffset)
            }
            if (x < width - 1) {
              if (voxels(i + rightOffset) == 0) vertexOffset = createRight(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createRight(offset, x, y, z, vertices, vertexOffset)
            }
            if (z > 0) {
              if (voxels(i + frontOffset) == 0) vertexOffset = createFront(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createFront(offset, x, y, z, vertices, vertexOffset)
            }
            if (z < depth - 1) {
              if (voxels(i + backOffset) == 0) vertexOffset = createBack(offset, x, y, z, vertices, vertexOffset)
            }
            else {
              vertexOffset = createBack(offset, x, y, z, vertices, vertexOffset)
            }
            println(vertexOffset)
          }
          i += 1
        }
      }
    }
    vertexOffset
  }

  def createTop(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertexOffsetTemp
  }

  def createBottom(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertexOffsetTemp
  }

  def createLeft(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertexOffsetTemp
  }

  def createRight(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertexOffsetTemp
  }

  def createFront(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 1
    vertexOffsetTemp
  }

  def createBack(offset: Vector3, x: Int, y: Int, z: Int, vertices: Array[Float], vertexOffset: Int): Int = {
    var vertexOffsetTemp = vertexOffset
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1

    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.x + x + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.y + y
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = offset.z + z + 1
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = 0
    vertices({vertexOffsetTemp += 1; vertexOffsetTemp}) = -1
    vertexOffsetTemp
  }
}