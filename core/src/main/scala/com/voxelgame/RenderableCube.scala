package com.voxelgame

import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.{GL20, Color, VertexAttribute, Mesh}
import com.badlogic.gdx.graphics.g3d.{Material, Renderable, RenderableProvider}
import com.badlogic.gdx.utils

class RenderableCube extends RenderableProvider {
  val mesh: Mesh = new Mesh(true, 36, 12, VertexAttribute.Position(), VertexAttribute.Normal())
  val vertices: Array[Float] = Array.fill(36)(0)
  for (i <- 0 until 36) {
    vertices.update(i, i)
  }
  val indices: Array[Short] = new Array[Short](12)
  for (i <- 0 until 12) {
    indices.update(i, i.toShort)
  }
  val material: Material = new Material(ColorAttribute.createDiffuse(Color.YELLOW))

  //mesh.setVertices(vertices)
  //mesh.setIndices(indices)
  mesh.setVertices(Array(-0.5f, -0.5f, 0,
    0.5f, -0.5f, 0,
    0, 0.5f, 0))

  mesh.setIndices(Array[Short](0, 1, 2));

  override def getRenderables(renderables: utils.Array[Renderable], pool: utils.Pool[Renderable]): Unit = {
    val renderable: Renderable = pool.obtain()
    renderable.mesh = mesh
    renderable.material = material
    renderable.meshPartOffset = 0
    renderable.meshPartSize = 36
    renderable.primitiveType = GL20.GL_LINES
    renderables.add(renderable)
  }
}
