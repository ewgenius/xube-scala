package com.voxelgame

import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g3d.{Material, RenderableProvider, Renderable}
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.utils

/**
 *
 * @param voxelSize
 * @param chunkSizeX
 * @param chunkSizeY
 * @param chunkSizeZ
 */
class VoxelNode(
                 private val voxelSize: Float,
                 private val chunkSizeX: Int,
                 private val chunkSizeY: Int,
                 private val chunkSizeZ: Int,
                 private val chunksX: Int,
                 private val chunksY: Int,
                 private val chunksZ: Int
                 ) extends RenderableProvider {
  val voxelsX = chunksX * chunkSizeX
  val voxelsY = chunksY * chunkSizeY
  val voxelsZ = chunksZ * chunkSizeZ

  val chunks: Array[VoxelChunk] = new Array[VoxelChunk](chunksX * chunksY * chunksZ)
  var i = 0
  for (y <- 0 until chunksY) {
    for (z <- 0 until chunksZ) {
      for (x <- 0 until chunksX) {
        val chunk = new VoxelChunk(chunkSizeX, chunkSizeY, chunkSizeZ)
        chunk.offset.set(x * chunkSizeX, y * chunkSizeY, z * chunkSizeZ)
        chunks.update(i, chunk)
        i += 1
      }
    }
  }

  val meshes: Array[Mesh] = new Array[Mesh](chunksX * chunksY * chunksZ)

  val indices: Array[Short] = new Array(chunkSizeX * chunkSizeY * chunkSizeZ * 36 / 3)
  var j = 0
  for (i <- 0 until chunkSizeX * chunkSizeY * chunkSizeZ * 36 / 3 by 6) {
    for (k <- 0 until 6)
      indices.update(i + k, (j + k).toShort)
  }
  for (i <- 0 until meshes.length) {
    meshes(i) = new Mesh(true,
      chunkSizeX * chunkSizeY * chunkSizeZ * 6 * 4,
      chunkSizeX * chunkSizeY * chunkSizeZ * 36 / 3,
      VertexAttribute.Position(), VertexAttribute.Normal())
    meshes(i).setIndices(indices)
  }

  val vertices: Array[Float] = new Array[Float](6 * 6 * chunkSizeX * chunkSizeY * chunkSizeZ)

  val numVertices: Array[Int] = new Array[Int](chunksX * chunksY * chunksZ)

  val materials: Array[Material] = new Array[Material](chunksX * chunksY * chunksZ)

  for (i <- 0 until materials.length)
    materials.update(i, new Material(ColorAttribute.createDiffuse(Color.YELLOW)))

  val dirty: Array[Boolean] = Array.fill(chunksX * chunksY * chunksZ)(true)

  def set(x: Float, y: Float, z: Float, voxel: Byte) {
    val ix: Int = x.toInt
    val iy: Int = y.toInt
    val iz: Int = z.toInt
    val chunkX: Int = ix / chunkSizeX
    if (chunkX < 0 || chunkX >= chunksX) return
    val chunkY: Int = iy / chunkSizeY
    if (chunkY < 0 || chunkY >= chunksY) return
    val chunkZ: Int = iz / chunkSizeZ
    if (chunkZ < 0 || chunkZ >= chunksZ) return
    chunks(chunkX + chunkZ * chunksX + chunkY * chunksX * chunksZ)
      .set(ix % chunkSizeX, iy % chunkSizeY, iz % chunkSizeZ, voxel)
  }

  def setCube(x: Float, y: Float, z: Float, width: Float, height: Float, depth: Float, voxel: Byte) {
    var ix: Int = x.toInt
    var iy: Int = y.toInt
    var iz: Int = z.toInt
    val iwidth: Int = width.toInt
    val iheight: Int = height.toInt
    val idepth: Int = depth.toInt
    val startX: Int = Math.max(ix, 0)
    val endX: Int = Math.min(voxelsX, ix + iwidth)
    val startY: Int = Math.max(iy, 0)
    val endY: Int = Math.min(voxelsY, iy + iheight)
    val startZ: Int = Math.max(iz, 0)
    val endZ: Int = Math.min(voxelsZ, iz + idepth)

    for (iy <- startY until endY) {
      for (iz <- startZ until endZ) {
        for (ix <- startX until endX) {
          set(ix, iy, iz, voxel)
        }
      }
    }
  }

  override def getRenderables(renderables: utils.Array[Renderable], pool: utils.Pool[Renderable]): Unit = {
    for (i <- 0 until chunks.length) {
      val chunk = chunks(i)
      val mesh = meshes(i)
      if (dirty(i)) {
        val numVerts = chunk.calculateVertices(vertices)
        numVertices.update(i, numVerts / 4 * 6)
        mesh.setVertices(vertices, 0, numVerts * 6)
        dirty.update(i, false)
      }
      if (numVertices(i) != 0) {
        val renderable: Renderable = pool.obtain()
        renderable.mesh = mesh
        renderable.material = materials(i)
        renderable.meshPartOffset = 0
        renderable.meshPartSize = numVertices(i)
        renderable.primitiveType = GL20.GL_TRIANGLES
        renderables.add(renderable)
      }
    }
  }
}